#!/usr/bin/env zsh

# Comment out the following line or change according to your setup
source $HOME/source.sh
rm -rf Process Result*
rm *.out

echo "Running LEP"
Sherpa -f Run.LEP.dat > LEP.out

rm -rf Process Result*

echo "Running LEP+2EPA"
Sherpa -f Run.LEP_EPA.dat > LEP_2EPA.out

rm -rf Process Result*

echo "Running LEP+1PDF"
Sherpa -f Run.LEP_1PDF.dat > LEP_1PDF.out

rm -rf Process Result*

echo "Running LEP+2PDF"
Sherpa -f Run.LEP_2PDF.dat > LEP_2PDF.out

#echo "Running LEP+EPA+1PDF"
# Does not work in v2.2.11

#echo "Running LEP+EPA+2PDF"
# Does not work in v2.2.11

rm -rf Process Result*

echo "Running HERA"
Sherpa -f Run.HERA.dat > HERA.out

rm -rf Process Result*

echo "Running HERA+PDF"
Sherpa -f Run.HERA_PDF.dat > HERA_PDF.out

rm -rf Process Result*

echo "Running HERA+EPA"
Sherpa -f Run.HERA_EPA.dat > HERA_EPA.out

#echo "Running HERA+EPA+PDF"
# Does not work in v2.2.11

rm -rf Process Result*

echo "Running LHC"
Sherpa -f Run.LHC.dat > LHC.out

rm -rf Process Re*

echo "Running LEP+5Jets"
Sherpa -f Run.LEP_5Jets.dat > LEP_5Jets.out

rm -rf Process Re*

echo "Running LHC_Jets_MEPS"
Sherpa -f Run.LHC_Jets_MEPS.dat > LHC_Jets_MEPS.out

rm -rf Process Re*

echo "Running LHC_WJets"
#Sherpa -f Run.LHC_WJets.dat;
#./makelibs
#Sherpa -f Run.LHC_WJets.dat > LHC_WJets.out

rm -rf Process Re*

echo "Running LO_Z"
Sherpa -f Run.LO_Z.dat > LO_Z.out

exit 0
