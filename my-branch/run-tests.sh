#!/usr/bin/env zsh

# Comment out the following line or change according to your setup
source $HOME/source.sh
rm -rf Process Result*
rm *.out

echo "Running LEP"
Sherpa "BEAM_SPECTRA: Monochromatic" "PDF_LIBRARY: None" "PDF_SET: None" > LEP.out

rm -rf Process Re*

echo "Running LEP+2EPA"
Sherpa "BEAM_SPECTRA: EPA" "EPA_LO: true" "EPA_LO: true" "PDF_LIBRARY: None" "PDF_SET: None" > LEP_2EPA.out

rm -rf Process Re*

echo "Running LEP+1PDF"
Sherpa -R 12345 "BEAM_SPECTRA: Monochromatic" "PDF_LIBRARY: [None, PDFESherpa]" "PDF_SET: [None, PDFe]" > LEP_1PDF.out

rm -rf Process Re*

echo "Running LEP+2PDF"
Sherpa -R 12345 "BEAM_SPECTRA: Monochromatic" "PDF_LIBRARY: PDFESherpa" "PDF_SET: PDFe" > LEP_2PDF.out

rm -rf Process Re*

echo "Running LEP+2EPA+1PDF"
Sherpa "BEAM_SPECTRA: EPA" "PDF_LIBRARY: SASGSherpa" "PDF_SET: [None,SAS1D]" > LEP_2EPA_1PDF.out

rm -rf Process Re*

echo "Running LEP+2EPA+2PDF"
Sherpa "BEAM_SPECTRA: EPA" "PDF_LIBRARY: SASGSherpa" "PDF_SET: SAS1D" > LEP_2EPA_2PDF.out

rm -rf Process Re*

echo "Running HERA"
Sherpa "BEAM_ENERGIES: [27.5, 920]" "BEAMS: [11, 2212]" "BEAM_SPECTRA: Monochromatic" "PDF_LIBRARY: [None,CTEQ6Sherpa]" "PDF_SET: [None,cteq6m]" > HERA.out

rm -rf Process Re*

echo "Running HERA+PDF"
Sherpa "BEAM_ENERGIES: [27.5, 920]" "BEAMS: [11, 2212]" "BEAM_SPECTRA: Monochromatic" "PDF_LIBRARY: [PDFESherpa,CTEQ6Sherpa]" "PDF_SET: [PDFe,cteq6m]" > HERA_PDF.out

rm -rf Process Re*

echo "Running HERA+EPA"
Sherpa "BEAM_ENERGIES: [27.5, 920]" "BEAMS: [11, 2212]" "BEAM_SPECTRA: [EPA,Monochromatic]" "PDF_LIBRARY: [None,CTEQ6Sherpa]" "PDF_SET: [None,cteq6m]" > HERA_EPA.out

rm -rf Process Re*

echo "Running HERA+EPA+PDF"
Sherpa "BEAM_ENERGIES: [27.5, 920]" "BEAMS: [11, 2212]" "BEAM_SPECTRA: [EPA,Monochromatic]" "PDF_LIBRARY: [SASGSherpa,CTEQ6Sherpa]" "PDF_SET: [SAS1D,cteq6m]" > HERA_EPA_PDF.out

rm -rf Process Re*

echo "Running LHC"
Sherpa -R 123456 "BEAM_ENERGIES: 6500" "BEAMS: 2212" "BEAM_SPECTRA: Monochromatic" "PDF_LIBRARY: CTEQ6Sherpa" "PDF_SET: cteq6m" > LHC.out

rm -rf Process Re*

echo "Running LEP+5Jets"
Sherpa -f Sherpa.LEP_5Jets.yaml > LEP_5Jets.out

rm -rf Process Re*

echo "Running LHC_Jets_MEPS"
Sherpa -f Sherpa.LHC_Jets_MEPS.yaml > LHC_Jets_MEPS.out

rm -rf Process Re*

echo "Running LHC_WJets"
#Sherpa -f Sherpa.LHC_WJets.yaml;
#./makelibs
#Sherpa -f Sherpa.LHC_WJets.yaml > LHC_WJets.out

rm -rf Process Re*

echo "Running LO_Z"
Sherpa -f Sherpa.LO_Z.yaml > LO_Z.out

exit 0
