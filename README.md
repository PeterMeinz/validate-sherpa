# Validation of Sherpa: comparison of XS between branch 327 and v2.2.11

This project is made to run different version of Sherpa and then analyse the XS in a Python notebook.
In this notebook, the XS of the integration and the event generation of each run are saved in a dictionary.

The setup in my-branch is made for Sherpa v3, i.e. yaml input, whereas v2.2.11 has tests which can be used for Sherpa v2 and earlier.

## Usage

Each subfolder should contains a `run-tests.sh` file and the corresponding input and output file(s).

As the name suggests, the .sh file has to be run to re-generate the output files from the runs.
To run on different environments, it sources a file `~/source.sh`.
This is obviously specific for my setup, so change it according to your environment, if you want to reproduce the tests.

After running the tests in the subfolders, open the Jupyter notebook and run all the cells.
This should give you the dictionary, which you can then use to plot, e.g., the differences between the XS.

**Note:** This is still work in progress.

TODOs:
- Extend each run to more examples
- For some reason, v2.2.11 does not accept the scale from the input file. Need to check this
